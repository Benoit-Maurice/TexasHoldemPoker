﻿using System;
using System.Collections.Generic;
using System.Linq;
using TexasHoldemPoker.Cards;

using static TexasHoldemPoker.Tests.Framework.CardParsingPattern<TexasHoldemPoker.Cards.Rank>;
using static TexasHoldemPoker.Tests.Framework.CardParsingPattern<TexasHoldemPoker.Cards.Suit>;

namespace TexasHoldemPoker.Tests.Framework
{
    public class StringToCardParser
    {
        private static readonly List<CardParsingPattern<Rank>> RankPatterns = new List<CardParsingPattern<Rank>>
        {
            CardParsingPattern<Rank>.Predicate(IsLessThanElevenAndGreaterThanOne, ConvertNumberToRank),
            Constant("T", Rank.Ten),
            Constant("J", Rank.Jack),
            Constant("Q", Rank.Queen),
            Constant("K", Rank.King),
            Constant("A", Rank.Ace)
        };

        private static readonly List<CardParsingPattern<Suit>> SuitPatterns = new List<CardParsingPattern<Suit>>
        {
            Constant("H", Suit.Heart),
            Constant("♥", Suit.Heart),

            Constant("D", Suit.Diamond),
            Constant("♦", Suit.Diamond),

            Constant("S", Suit.Spade),
            Constant("♠", Suit.Spade),

            Constant("C", Suit.Club),
            Constant("♣", Suit.Club)
        };

        private readonly Deck deck;

        public StringToCardParser(Deck deck)
        {
            this.deck = deck;
        }

        public Card Parse(string cardAsString)
        {
            CheckString(cardAsString);

            var rankLength = cardAsString.Length - 1;

            var rankAsString = cardAsString.Substring(0, rankLength);
            var rank = ParseRank(rankAsString);

            var suitAsString = cardAsString.Substring(rankLength, 1);
            var suit = ParseSuit(suitAsString);

            var found = deck.FindCard(rank, suit);
            return found;
        }

        private static Rank ConvertNumberToRank(string number)
        {
            return (Rank) int.Parse(number);
        }

        private static Rank ParseRank(string rankAsString)
        {
            var matchingPattern = RankPatterns.Where(pattern => pattern.Matches(rankAsString)).ToList();
            if (!matchingPattern.Any())
            {
                throw new Exception($"Unknown rank : {rankAsString}");
            }

            return matchingPattern.First().GetValue(rankAsString);
        }

        private static bool IsLessThanElevenAndGreaterThanOne(string rankAsString)
        {
            return int.TryParse(rankAsString, out var rankValue) &&
                   rankValue >= 2 && rankValue <= 10;
        }

        private static Suit ParseSuit(string suitAsString)
        {
            var matchingPattern = SuitPatterns.Where(pattern => pattern.Matches(suitAsString)).ToList();
            if (!matchingPattern.Any())
            {
                throw new Exception($"Unknown suit : {suitAsString}");
            }

            return matchingPattern.First().GetValue(suitAsString);
        }

        private static void CheckString(string cardAsString)
        {
            if (string.IsNullOrEmpty(cardAsString))
            {
                throw new Exception("Provided string is empty. Can't parse anything.");
            }

            if (cardAsString.Length < 2)
            {
                throw new Exception(
                    $"Provided string '{cardAsString}' is way too short. It should be 2 chars long min.");

            }

            if (cardAsString.Length > 3)
            {
                throw new Exception(
                    $"Provided string '{cardAsString}' is way too long. It should be 3 chars long max.");

            }
        }  
    }
}