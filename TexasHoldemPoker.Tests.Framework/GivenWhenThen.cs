﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using NFluent;
using TexasHoldemPoker.Framework;
using TexasHoldemPoker.Tests.Framework.ScenarioGeneration;

namespace TexasHoldemPoker.Tests.Framework
{
    public class Given
    {
        private readonly Func<string> getScenarioName;
        private readonly IExportAScenario[] exporters;

        public Given(Func<string> getScenarioName, params IExportAScenario[] exporters)
        {
            this.getScenarioName = getScenarioName;
            this.exporters = exporters;
        }

        public Given<TAggregate> A<TAggregate>()
            where TAggregate : Aggregate
        {
            return new Given<TAggregate>(getScenarioName(), exporters);
        }
    }

    public class Given<TAggregate>
        where TAggregate : Aggregate
    {
        private readonly InMemoryEventStore eventStore = new InMemoryEventStore();
        private readonly Scenario scenario;

        [DebuggerHidden]
        internal Given(string scenarioName, params IExportAScenario[] exporters)
        {
            scenario = new Scenario(scenarioName, exporters);
            scenario.Given(Step.FromType<TAggregate>());
        }

        [DebuggerHidden]
        public Given<TAggregate> WithHistory(Func<History> pastEvents)
        {
            return WithHistory(pastEvents().ToArray());
        }

        [DebuggerHidden]
        public Given<TAggregate> And(Func<History> pastEvents)
        {
            return WithHistory(pastEvents);
        }

        [DebuggerHidden]
        public Given<TAggregate> WithHistory(params DomainEvent[] pastEvents)
        {
            scenario.WithHistory(pastEvents.Select(Step.FromInstance));
            eventStore.Add(pastEvents);
            return this;
        }

        [DebuggerHidden]
        public Given<TAggregate> And(params DomainEvent[] pastEvents)
        {
            return WithHistory(pastEvents);
        }

        [DebuggerHidden]
        public When<TAggregate> When(New<TAggregate> command)
        {
            return When<TAggregate>.New(command, eventStore, eventStore, scenario);
        }

        [DebuggerHidden]
        public When<TAggregate> When(Do<TAggregate> command)
        {
            return When<TAggregate>.Do(command, eventStore, eventStore, scenario);
        }
    }

    public class When<TAggregate>
        where TAggregate : Aggregate
    {
        private readonly Action dispatch;

        internal static When<TAggregate> New(New<TAggregate> command, ILogEvents logEvents, ILoadHistory loadHistory, Scenario scenario)
        {
            var dispatcher = new Dispatcher(new StorageFromHistory(loadHistory, logEvents));
            scenario.When(Step.FromInstance(command));
            return new When<TAggregate>(() => dispatcher.Dispatch(command), command.AggregateId, loadHistory, scenario);
        }

        internal static When<TAggregate> Do(Do<TAggregate> command, ILogEvents logEvents, ILoadHistory loadHistory, Scenario scenario)
        {
            var dispatcher = new Dispatcher(new StorageFromHistory(loadHistory, logEvents));
            scenario.When(Step.FromInstance(command));
            return new When<TAggregate>(() => dispatcher.Dispatch(command), command.AggregateId, loadHistory, scenario);
        }


        private readonly object aggregateId;

        private readonly ILoadHistory loadHistory;
        private readonly Scenario scenario;

        [DebuggerHidden]
        internal When(Action dispatch, object aggregateId, ILoadHistory loadHistory, Scenario scenario)
        {
            this.dispatch = dispatch;
            this.loadHistory = loadHistory;
            this.scenario = scenario;
            this.aggregateId = aggregateId;
        }

        [DebuggerHidden]
        public void Then(CheckNewHistory checkNew)
        {
            History previousHistory = loadHistory.HistoryOf(aggregateId);

            var previousHistorySize = previousHistory.Count();
            dispatch();

            var newHistory = new History(loadHistory.HistoryOf(aggregateId)
                .Skip(previousHistorySize));

            try
            {
                checkNew.Evaluate(newHistory);
            }
            finally
            {
                scenario.Then(checkNew.Steps)
                        .Export();
            }
        }

        [DebuggerHidden]
        public void ThenItFailsWith<TError>(string message = null)
            where TError : Exception
        {
            var error = Check.ThatCode(() => dispatch()).Throws<TError>();

            if (!string.IsNullOrEmpty(message))
            {
                error.WithMessage(message);
            }

            scenario.Then(Step.FromString($"it fails: {error.Value.Message}"))
                .Export();
        }

        [DebuggerHidden]
        public void ThenItDoesNotFailWith<TError>()
            where TError : Exception
        {
            Check.ThatCode(() => dispatch()).Not.Throws<TError>();

            scenario.Then(Step.FromString($"it does not fails"))
                .Export();
        }
    }

    public class CheckNewHistory
    {
        private readonly Func<History, ICheck<History>> that;
        private readonly Action<ICheck<History>> check;

        public IEnumerable<Step> Steps { get; }

        [DebuggerHidden]
        private CheckNewHistory(IEnumerable<Step> steps, Action<ICheck<History>> check)
            : this(steps, Check.That, check)
        {
        }

        [DebuggerHidden]
        private CheckNewHistory(IEnumerable<Step> steps, Func<History, ICheck<History>> that, Action<ICheck<History>> check)
        {
            Steps = steps;
            this.that = that;
            this.check = check;
        }

        [DebuggerHidden]
        public void Evaluate(History actual)
        {
            check(that(actual));
        }

        [DebuggerHidden]
        public static CheckNewHistory Is(params DomainEvent[] domainEvents)
        {
            var steps = new List<Step>
            {
                Step.FromString($"history has {domainEvents.Length} events")
            };
            steps.AddRange(domainEvents.Select(Step.FromInstance));

            return new CheckNewHistory(steps, that => that.Equals(domainEvents));
        }

        [DebuggerHidden]
        public static CheckNewHistory StartsWith(params DomainEvent[] domainEvents)
        {
            var steps = new List<Step>
            {
                Step.FromString($"history starts with {domainEvents.Length}")
            };
            steps.AddRange(domainEvents.Select(Step.FromInstance));

            return new CheckNewHistory(steps, that => that.StartsWith(domainEvents));
        }

        [DebuggerHidden]
        public static CheckNewHistory EndsWith(params DomainEvent[] domainEvents)
        {
            var steps = new List<Step>
            {
                Step.FromString($"history ends with {domainEvents.Length} events")
            };
            steps.AddRange(domainEvents.Select(Step.FromInstance));

            return new CheckNewHistory(steps, that => that.EndsWith(domainEvents));
        }

        [DebuggerHidden]
        public static CheckHistorySlice Skip(int from)
        {
            return CheckHistorySlice.From(from);
        }

        public class CheckHistorySlice
        {
            private readonly int start;

            [DebuggerHidden]
            public static CheckHistorySlice From(int from)
            {
                return new CheckHistorySlice(from);
            }

            [DebuggerHidden]
            private CheckHistorySlice(int start)
            {
                this.start = start;
            }

            [DebuggerHidden]
            public CheckNewHistory Equals(params DomainEvent[] domainEvents)
            {
                var steps = new List<Step>
                {
                    Step.FromString($"after {start} events, history has {domainEvents.Length} events")
                };

                steps.AddRange(domainEvents.Select(Step.FromInstance));

                return new CheckNewHistory(
                    steps,
                    actual => Check.That(Slice(actual, start, domainEvents.Length)),
                    that => that.Equals(domainEvents)
                    );
            }



            [DebuggerHidden]
            private static History Slice(History actual, int start, int count)
            {
                return new History(actual.Skip(start)
                    .Take(count));
            }
        }
    }
}