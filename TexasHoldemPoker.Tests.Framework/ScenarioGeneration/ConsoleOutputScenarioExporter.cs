﻿using System;
using System.Collections.Generic;

namespace TexasHoldemPoker.Tests.Framework.ScenarioGeneration
{
    public class TextScenarioExporter : IExportAScenario
    {
        private readonly Action<string> writeLineToConsole;

        public TextScenarioExporter(Action<string> writeLine)
        {
            this.writeLineToConsole = writeLine;
        }

        public IExportAScenario WriteGiven(IEnumerable<Step> givenSteps)
        {
            givenSteps.WrittenAs("Given").ForEach(writeLineToConsole);
            return this;
        }

        public IExportAScenario WriteName(string name)
        {
            writeLineToConsole($"Scenario: {name}");
            return this;
        }

        public IExportAScenario WriteThen(IEnumerable<Step> thenSteps)
        {
            thenSteps.WrittenAs("Then").ForEach(writeLineToConsole);
            return this;
        }

        public IExportAScenario WriteWhen(Step whenStep)
        {
            writeLineToConsole(whenStep.WrittenAs("When"));
            return this;
        }
    }
}
