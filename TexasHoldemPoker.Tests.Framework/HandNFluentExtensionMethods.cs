﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NFluent;
using NFluent.Extensibility;
using TexasHoldemPoker.Cards;
using TexasHoldemPoker.Hands.HandValues;

namespace TexasHoldemPoker.Tests.Framework
{
    public static class HandNFluentExtensionMethods
    {
        public static ICheckLink<ICheck<HandValue>> Beats(
            this ICheck<HandValue> check,
            HandValue otherEvaluatedHand)
        {
            var runnableCheck = ExtensibilityHelper.ExtractChecker(check);
            var evaluatedHand = runnableCheck.Value;

            return runnableCheck.ExecuteCheck(() =>
            {
                if (!evaluatedHand.Beats(otherEvaluatedHand))
                {
                    throw new FluentCheckException($"{evaluatedHand} does not beat {otherEvaluatedHand}");
                }
            }, $"{evaluatedHand} should have beaten {otherEvaluatedHand}");
        }

        public static ICheckLink<ICheck<IEnumerable<HandValue>>> Beats<THandValue1, THandValue2>(
            this ICheck<List<THandValue1>> check,
            List<THandValue2> othersEvaluatedHands, int hands1SplitChunkSize = 1000, int hands2SplitChunkSize = 1000)
            where THandValue1 : HandValue
            where THandValue2 : HandValue
        {
            var runnableCheck = ExtensibilityHelper.ExtractChecker(check);
            var evaluatedHandsChunks = runnableCheck.Value.Cast<HandValue>().ToArray().Split(hands1SplitChunkSize);
            var otherEvaluatedHandsChunks =
                othersEvaluatedHands.Cast<HandValue>().ToArray().Split(hands2SplitChunkSize);

            return runnableCheck.ExecuteCheck(() =>
            {
                var errors = new ConcurrentBag<string>();

                var tokenSource = new CancellationTokenSource();
                var token = tokenSource.Token;
                var tasks = new Task[evaluatedHandsChunks.Length * otherEvaluatedHandsChunks.Length];
                var taskIndex = 0;

                foreach (var evaluatedHandsChunk in evaluatedHandsChunks)
                {
                    foreach (var otherEvaluatedHandChunk in otherEvaluatedHandsChunks)
                    {
                        var chunk = evaluatedHandsChunk;
                        var handChunk = otherEvaluatedHandChunk;
                        tasks[taskIndex++] = Task.Factory.StartNew(() =>
                        {
                            if (token.IsCancellationRequested)
                            {
                                return;
                            }

                            foreach (var evaluatedHand in chunk)
                            {
                                if (token.IsCancellationRequested)
                                {
                                    return;
                                }

                                foreach (var t in handChunk)
                                {
                                    if (token.IsCancellationRequested)
                                    {
                                        return;
                                    }

                                    var otherEvaluatedHand = t;

                                    if (evaluatedHand.Beats(otherEvaluatedHand))
                                    {
                                        continue;
                                    }
                                    errors.Add($"{evaluatedHand} does not beat {otherEvaluatedHand}");

                                    if (errors.Count > 10)
                                    {
                                        tokenSource.Cancel(false);
                                    }
                                }
                            }
                        }, token);
                    }
                }

                try
                {
                    Task.WaitAll(tasks, token);
                }
                catch (OperationCanceledException) { }
                finally
                {
                    if (errors.Any())
                    {
                        var stringBuilder = new StringBuilder();
                        errors.ToList()
                            .ForEach(e => stringBuilder.AppendLine(e));
                        throw new FluentCheckException(stringBuilder.ToString());
                    }
                }
            }, " ");
        }

        public static ICheckLink<ICheck<Card>> Is(
            this ICheck<Card> check,
            Card card)
        {
            return check.IsEqualTo(card);
        }
    }
}