﻿using System.Linq;

namespace TexasHoldemPoker.Tests.Framework
{
    public static class ArrayExtensionMethods
    {
        public static T[] Plus<T>(this T[] items, T[] otherItems)
        {
            return items.Union(otherItems).ToArray();
        }

        public static T[] Plus<T>(this T[] items, T item)
        {
            return items.Plus(new[] { item });
        }
    }
}
