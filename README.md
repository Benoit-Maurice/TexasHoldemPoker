TEXAS HOLD'EM POKER Sandbox
===========================

This projet was initially about the [Texas Hold'Em Poker Kata][kata] from [CodingDojo.org][coding dojo]

It soon became a big sand box for :
- the study of .net framework and solutions to improve performances such as
    + the proper use of structs
    + avoid too many loops
    + use for loops instead of foreach/linq loops when optimization is necessary
    + use arrays instead of lists
- an attempt to use [Property Based Testing][property based testing] to test the comparison of two hands
- (...In progress...) An implementation of the [CQRS pattern][cqrs]
- Simplify ValueObjects implementation using [Value][value]
- (...Coming soon...) [RESTFul][restful] API using
    + [ASP.NET Core][aspnetcore] ?
    + [NancyFx][nancyfx] ?
- (...Coming soon...)  Client side frameworks such as 
    + [AngularJs][angularjs] ?
    + [ReactJs][reactjs] ?
    + [Aurelia][aurelia] ?

[kata]:http://codingdojo.org/cgi-bin/index.pl?KataTexasHoldEm
[coding dojo]:http://codingdojo.org
[property based testing]:https://www.youtube.com/watch?v=O-LWbSUaEQU
[cqrs]:http://martinfowler.com/bliki/CQRS.html
[restful]:https://en.wikipedia.org/wiki/Representational_state_transfer
[angularjs]:https://angularjs.org
[reactjs]:https://facebook.github.io/react/
[aurelia]:http://aurelia.io/
[aspnetcore]:https://docs.microsoft.com/en-us/aspnet/core/
[nancyfx]:http://nancyfx.org/
[value]:https://github.com/tpierrain/Value