﻿using System;
using TexasHoldemPoker.Cards;

namespace TexasHoldemPoker.Hands.HandValues
{
    public abstract class CardGroupsHandValue : HandValue
    {
        private readonly int groupSize;

        public CardGroupsHandValue(Hand hand, Rank rank, HighCard remainingCards, int groupSize)
            : base(hand)
        {
            Rank = rank;
            RemainingCards = remainingCards;
            this.groupSize = groupSize;
        }

        public Rank Rank { get; }

        public HighCard RemainingCards { get; }

        protected override long ComputeHandStrength()
        {
            return (long) Rank * (long) Math.Pow(14, Hand.Size - groupSize) + RemainingCards.FullStrength;
        }

        public override string ToString()
        {
            return base.ToString() + $" of {Rank}";
        }
    }
}