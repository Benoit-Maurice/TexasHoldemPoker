﻿using System;
using System.Collections.Generic;
using Value;

namespace TexasHoldemPoker.Tables
{
    public class TableId : ValueType<TableId>
    {
        private readonly string id;

        public static TableId New(string id = null)
        {
            return new TableId(id ?? Guid.NewGuid().ToString());
        }

        private TableId(string id)
        {
            this.id = id;
        }

        protected override IEnumerable<object> GetAllAttributesToBeUsedForEquality()
        {
            return new[] { id };
        }

        public override string ToString()
        {
            return $"#Table{id}";
        }


    }
}