﻿using TexasHoldemPoker.Players;
using TexasHoldemPoker.Framework;

namespace TexasHoldemPoker.Tables.Commands
{
    public class PlayerJoins : Do<Table>
    {
        public TableId TableId { get; }
        public PlayerId PlayerId { get; }
        public decimal Stack { get; }

        public object AggregateId => TableId;

        public PlayerJoins(TableId tableId, PlayerId playerId, decimal stack)
        {
            TableId = tableId;
            PlayerId = playerId;
            Stack = stack;
        }
    }
}