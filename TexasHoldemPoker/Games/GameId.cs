﻿using System.Collections.Generic;
using Value;

namespace TexasHoldemPoker.Tables
{
    public class GameId : ValueType<GameId>
    {
        public TableId TableId { get; }
        public int Number { get; }

        public static GameId New(TableId tableId, int number=0)
        {
            return new GameId(tableId, number);
        }

        private GameId(TableId tableId, int number)
        {
            TableId = tableId;
            Number = number;
        }

        public GameId Next()
        {
            return new GameId(TableId, Number+1);
        }

        protected override IEnumerable<object> GetAllAttributesToBeUsedForEquality()
        {
            return new object[] { TableId, Number };
        }

        public override string ToString()
        {
            return $"Game#{Number}-{TableId}";
        }

    }
}