﻿using System.Collections.Generic;
using TexasHoldemPoker.Hands;
using TexasHoldemPoker.Players;

namespace TexasHoldemPoker.Games.Domain
{
    public class InGameHands
    {
        private readonly Dictionary<PlayerId, Hand> hands = new Dictionary<PlayerId, Hand>();

        public Hand this[PlayerId playerId]
        {
            get
            {
                if (!hands.ContainsKey(playerId))
                {
                    hands[playerId] = new Hand(2);
                }

                return hands[playerId];
            }
        }
    }
}