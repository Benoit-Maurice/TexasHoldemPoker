﻿using System.Collections.Generic;
using System.Linq;
using TexasHoldemPoker.Players;

namespace TexasHoldemPoker.Games.Domain
{
    public class Bets
    {
        private readonly Dictionary<PlayerId, Bet> bets = new Dictionary<PlayerId, Bet>();

        public Bet LastBet { get; private set; }
        public int LastRaise { get; private set; }

        public void Place(Bet bet)
        {
            bets[bet.PlayerId] = bet;
            var previousBet = LastBet;

            LastBet = bet;

            LastRaise = previousBet != null ? 
                            bet.Value - previousBet.Value : 
                            bet.Value;
        }

        public bool HasPlayer(PlayerId playerId)
        {
            return bets.ContainsKey(playerId);
        }

        public Bet this[PlayerId playerId] => bets[playerId];

        internal Dictionary<PlayerId, int> ToDictionary()
        {
            return bets.ToDictionary(b => b.Key, b => b.Value.Value);
        }

        internal void Clean()
        {
            bets.Clear();
            LastBet = null;
            LastRaise = default;
        }
    }
}