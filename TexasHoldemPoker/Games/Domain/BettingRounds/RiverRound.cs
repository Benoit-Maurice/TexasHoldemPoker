﻿using System;
using TexasHoldemPoker.Cards;
using TexasHoldemPoker.Games.Events;
using TexasHoldemPoker.Players;
using TexasHoldemPoker.Tables;
using TexasHoldemPoker.Framework;

namespace TexasHoldemPoker.Games.Domain.BettingRounds
{
    internal class RiverRound : BettingRound
    {
        public RiverRound(
            GameId gameId, Deck deck, Bets bets,
            PlayerId dealerId, IEnumeratePlayerIds players, Action<DomainEvent> fire)
            : base(gameId, deck, bets, dealerId, players, fire)
        {
        }

        public override BettingRoundName NextBettingRoundName => BettingRoundName.Showdown;

        public override BettingRoundName CurrentBettingRoundName => BettingRoundName.River;

        protected override void OnStart()
        {
            DrawRiver();
        }

        private void DrawRiver()
        {
            Fire(new CardBurnt(GameId, Deck.Top));
            Fire(new RiverDealt(GameId, Deck.Top));
        }
    }
}