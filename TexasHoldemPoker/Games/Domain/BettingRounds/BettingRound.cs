﻿using System;
using System.Linq;
using TexasHoldemPoker.Cards;
using TexasHoldemPoker.Games.Events;
using TexasHoldemPoker.Players;
using TexasHoldemPoker.Tables;
using TexasHoldemPoker.Framework;

using static TexasHoldemPoker.Games.Domain.PlayerMoveType;

namespace TexasHoldemPoker.Games.Domain.BettingRounds
{
    public abstract class BettingRound
    {
        protected readonly GameId GameId;
        protected readonly Deck Deck;
        protected readonly Bets Bets;
        protected readonly PlayerId DealerId;
        protected readonly IEnumeratePlayerIds Players;
        protected readonly Action<DomainEvent> Fire;

        protected BettingRound(GameId gameId, Deck deck, Bets bets,
            PlayerId dealerId,
            IEnumeratePlayerIds players,
            Action<DomainEvent> fire)
        {
            GameId = gameId;
            Deck = deck;
            Bets = bets;
            DealerId = dealerId;
            Players = players;
            Fire = fire;
        }

        protected void StartPlayerTurn(PlayerId playerId)
        {
            Fire(new PlayerTurnStarted(GameId, playerId));
        }

        public virtual BettingRoundStatus Continue()
        {
            if (!Started())
            {
                StartBettingRound();
                OnStart();
            }

            if (IsOver())
            {
                EndBettingRound();
                return BettingRoundStatus.Over;
            }

            StartNextPlayerTurn();
            return BettingRoundStatus.Pending;
        }

        protected abstract void OnStart();

        protected virtual void StartBettingRound()
        {
            Fire(new BettingRoundStarted(GameId, CurrentBettingRoundName, DealerId));
        }

        private void EndBettingRound()
        {
            Fire(new BettingRoundIsOver(GameId, CurrentBettingRoundName, Bets.ToDictionary()));
        }

        public abstract BettingRoundName NextBettingRoundName { get; }

        public abstract BettingRoundName CurrentBettingRoundName { get; }

        private bool Started()
        {
            var allPlayers = Players.Select(pid => Players[pid]);
            var aPlayerHasMovedThisRound = allPlayers.Any(p => p.HasMoved(CurrentBettingRoundName));
            return aPlayerHasMovedThisRound;
        }

        protected virtual bool IsOver()
        {
            if(LessThanOnePlayerCanMove() && NobodyMovedYet())
            {
                return true;
            }

            if (!AllPlayersHaveMovedThisRound())
            {
                return false;
            }

            return AllPlayerHaveCheckedThisRound()
            || OnePlayerBetAndOthersHaveCalledOrFolded()
            || OnePlayerRaisedAndOthersHaveCalledOrFolded()
            || OnePlayerWentAllInAndOthersHaveCalledOrFolded();
        }

        protected void StartNextPlayerTurn()
        {
            StartPlayerTurn(Players.NextId);
        }

        private bool LessThanOnePlayerCanMove()
        {
            var alivePlayers = Players.Select(pid => Players[pid]).Where(p => p.CanMove);
            var alivePlayersCount = alivePlayers.Count();
            return alivePlayersCount <= 1;
        }

        private bool NobodyMovedYet()
        {
            return Players.Select(pid => Players[pid]).All(p => !p.HasMoved(CurrentBettingRoundName));
        }

        private bool OnePlayerRaisedAndOthersHaveCalledOrFolded()
        {
            var raisePlayer = Players.SingleOrDefault(
                pid =>
                {
                    var p = Players[pid];
                    return p.LastMove == Raise;
                });

            if (raisePlayer == null)
            {
                return false;
            }

            return Players.Except(new[] { raisePlayer })
                          .All(pid =>
                          {
                              var p = Players[pid];
                              return
                                 p.LastMove == Fold ||
                                 p.LastMove == Call;
                          });
        }

        private bool OnePlayerBetAndOthersHaveCalledOrFolded()
        {
            var betPlayer = Players.SingleOrDefault(
                pid =>
                {
                    var p = Players[pid];
                    return p.LastMove == PlayerMoveType.Bet;
                });

            if (betPlayer == null)
            {
                return false;
            }

            return Players.Except(new[] { betPlayer })
                          .All(pid =>
                          {
                              var p = Players[pid];
                              return
                                 p.LastMove == Fold ||
                                 p.LastMove == Call;
                          });
        }

        private bool OnePlayerWentAllInAndOthersHaveCalledOrFolded()
        {
            var betPlayer = Players.SingleOrDefault(
                pid =>
                {
                    var p = Players[pid];
                    return p.LastMove == AllIn;
                });

            if (betPlayer == null)
            {
                return false;
            }

            return Players.Except(new[] { betPlayer })
                          .All(pid =>
                          {
                              var p = Players[pid];
                              return
                                 p.LastMove == Fold ||
                                 p.LastMove == Call;
                          });
        }

        private bool AllPlayersHaveMovedThisRound()
        {
            return Players.All(pid =>
            {
                var p = Players[pid];
                return !p.CanMove || p.HasMoved(CurrentBettingRoundName);
            });
        }

        private bool AllPlayerHaveCheckedThisRound()
        {
            return Players.All(pid =>
            {
                var p = Players[pid];
                return
                    p.LastMove == Fold ||
                    p.LastMove == Check;
            });
        }

    }
}