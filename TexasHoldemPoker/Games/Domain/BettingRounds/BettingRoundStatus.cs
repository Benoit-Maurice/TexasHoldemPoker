﻿namespace TexasHoldemPoker.Games.Domain.BettingRounds
{
    public enum BettingRoundStatus
    {
        Pending,
        Over
    }
}