﻿using System.Collections.Generic;
using TexasHoldemPoker.Players;
using Value;

namespace TexasHoldemPoker.Games.Domain
{
    public class Bet : ValueType<Bet>
    {
        public PlayerId PlayerId { get; }
        public int Value { get; }

        public static Bet New(PlayerId player, int value)
        {
            return new Bet(player, value);
        }

        private Bet(PlayerId playerId, int value)
        {
            PlayerId = playerId;
            Value = value;
        }

        protected override IEnumerable<object> GetAllAttributesToBeUsedForEquality()
        {
            return new object[] { PlayerId, Value };
        }
    }
}