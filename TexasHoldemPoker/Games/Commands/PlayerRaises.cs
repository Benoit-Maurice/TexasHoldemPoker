﻿using TexasHoldemPoker.Players;
using TexasHoldemPoker.Tables;
using TexasHoldemPoker.Framework;

namespace TexasHoldemPoker.Games.Commands
{
    public class PlayerRaises : Do<Game>
    {
        public object AggregateId { get; }
        public PlayerId PlayerId { get; }
        public int RaiseValue { get; }

        public PlayerRaises(GameId gameId, PlayerId playerId, int raiseValue)
        {
            AggregateId = gameId;
            PlayerId = playerId;
            RaiseValue = raiseValue;
        }
    }
}