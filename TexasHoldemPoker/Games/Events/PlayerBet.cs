﻿using System.Collections.Generic;
using TexasHoldemPoker.Players;
using TexasHoldemPoker.Tables;
using TexasHoldemPoker.Framework;
using Value;
using System.Linq;

namespace TexasHoldemPoker.Games.Events
{
    public class PlayerBet : DomainEvent<PlayerBet>
    {
        public PlayerId PlayerId { get; }
        public int BetValue { get; }

        public override object AggregateId { get; }

        public PlayerBet(object gameId, PlayerId playerId, int betValue)
        {
            AggregateId = gameId;
            PlayerId = playerId;
            BetValue = betValue;
        }

        protected override IEnumerable<object> GetAllAttributesToBeUsedForEquality()
        {
            return base.GetAllAttributesToBeUsedForEquality()
                .Union(new object[] { PlayerId, BetValue });
        }
    }
}