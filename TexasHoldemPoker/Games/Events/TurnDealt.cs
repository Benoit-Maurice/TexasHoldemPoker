﻿using System.Collections.Generic;
using TexasHoldemPoker.Cards;
using TexasHoldemPoker.Tables;
using TexasHoldemPoker.Framework;
using Value;
using System.Linq;

namespace TexasHoldemPoker.Games.Events
{
    public class TurnDealt : DomainEvent<TurnDealt>
    {
        public override object AggregateId { get; }
        public Card TurnCard { get; }

        public TurnDealt(GameId gameId, Card turnCard)
        {
            AggregateId = gameId;
            TurnCard = turnCard;
        }

        protected override IEnumerable<object> GetAllAttributesToBeUsedForEquality()
        {
            return base.GetAllAttributesToBeUsedForEquality().Union(new object[] { TurnCard });
        }
    }
}