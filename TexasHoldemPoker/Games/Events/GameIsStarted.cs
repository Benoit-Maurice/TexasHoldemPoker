﻿using System.Collections.Generic;
using System.Linq;
using TexasHoldemPoker.Cards;
using TexasHoldemPoker.Games.Domain;
using TexasHoldemPoker.Players;
using TexasHoldemPoker.Tables;
using TexasHoldemPoker.Framework;
using Value;
using Value.Shared;

namespace TexasHoldemPoker.Games.Events
{
    public class GameIsStarted : DomainEvent<GameIsStarted>
    {
        public override object AggregateId { get; }

        public GameId GameId { get; }

        public Card[] Cards { get; }
        public Blinds Blinds { get; }
        public IDictionary<PlayerId, int> Players { get; }
        public PlayerId DealerId { get; }

        public GameIsStarted(
            GameId gameId, Card[] cards, Blinds blinds,
            PlayerId dealerId, IDictionary<PlayerId, int> players)
        {
            AggregateId = gameId;
            GameId = gameId;
            Cards = cards;
            Blinds = blinds;
            DealerId = dealerId;
            Players = players;
        }

        protected override IEnumerable<object> GetAllAttributesToBeUsedForEquality()
        {
            return base.GetAllAttributesToBeUsedForEquality().Union(
            new object[]
            {
                new ListByValue<Card>(Cards),
                Blinds,
                DealerId,
                new DictionaryByValue<PlayerId, int>(Players),
            });
        }
    }
}