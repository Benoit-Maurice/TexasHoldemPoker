﻿using System.Collections.Generic;
using TexasHoldemPoker.Cards;
using TexasHoldemPoker.Tables;
using TexasHoldemPoker.Framework;
using Value;
using System.Linq;

namespace TexasHoldemPoker.Games.Events
{
    public class CardBurnt : DomainEvent<CardBurnt>
    {
        public override object AggregateId { get; }
        public Card Card { get; }

        public CardBurnt(GameId gameId, Card card)
        {
            AggregateId = gameId;
            Card = card;
        }

        protected override IEnumerable<object> GetAllAttributesToBeUsedForEquality()
        {
            return base.GetAllAttributesToBeUsedForEquality()
                .Union(new object[] { Card });
        }
    }
}