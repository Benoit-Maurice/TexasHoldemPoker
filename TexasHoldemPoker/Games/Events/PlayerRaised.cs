﻿using System.Collections.Generic;
using TexasHoldemPoker.Players;
using TexasHoldemPoker.Tables;
using TexasHoldemPoker.Framework;
using System.Linq;

namespace TexasHoldemPoker.Games.Events
{
    public class PlayerRaised : DomainEvent<PlayerRaised>
    {
        public override object AggregateId { get; }
        public PlayerId PlayerId { get; }
        public int RaiseValue { get; }

        public PlayerRaised(object gameId, PlayerId playerId, int raiseValue)
        {
            AggregateId = gameId;
            PlayerId = playerId;
            RaiseValue = raiseValue;
        }

        protected override IEnumerable<object> GetAllAttributesToBeUsedForEquality()
        {
            return base.GetAllAttributesToBeUsedForEquality().Union(new object[] { PlayerId, RaiseValue });
        }
    }
}