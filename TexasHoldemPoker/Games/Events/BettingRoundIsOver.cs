﻿using System.Collections.Generic;
using System.Linq;
using TexasHoldemPoker.Games.Domain.BettingRounds;
using TexasHoldemPoker.Players;
using TexasHoldemPoker.Tables;
using TexasHoldemPoker.Framework;
using Value;
using Value.Shared;

namespace TexasHoldemPoker.Games.Events
{
    public class BettingRoundIsOver : DomainEvent<BettingRoundIsOver>
    {
        public override object AggregateId { get; }
        public BettingRoundName Name { get; }
        public Dictionary<PlayerId, int> Bets { get; }

        public BettingRoundIsOver(
            GameId gameId, BettingRoundName name,
            Dictionary<PlayerId, int> bets)
        {
            AggregateId = gameId;
            Name = name;
            Bets = bets;
        }

        protected override IEnumerable<object> GetAllAttributesToBeUsedForEquality()
        {
            return base.GetAllAttributesToBeUsedForEquality()
                .Union(new object[] { Name, new DictionaryByValue<PlayerId, int>(Bets) });
        }
    }
}