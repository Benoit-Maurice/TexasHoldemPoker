﻿using System.Collections.Generic;
using TexasHoldemPoker.Cards;
using TexasHoldemPoker.Tables;
using TexasHoldemPoker.Framework;
using System.Linq;

namespace TexasHoldemPoker.Games.Events
{
    public class RiverDealt : DomainEvent<RiverDealt>
    {
        public override object AggregateId { get; }
        public Card RiverCard { get; }

        public RiverDealt(GameId gameId, Card river)
        {
            AggregateId = gameId;
            RiverCard = river;
        }

        protected override IEnumerable<object> GetAllAttributesToBeUsedForEquality()
        {
            return base.GetAllAttributesToBeUsedForEquality().Union(new object[] { RiverCard });
        }
    }
}