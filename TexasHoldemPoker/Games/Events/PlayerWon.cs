﻿using System.Collections.Generic;
using TexasHoldemPoker.Players;
using TexasHoldemPoker.Tables;
using TexasHoldemPoker.Framework;
using Value;
using System.Linq;

namespace TexasHoldemPoker.Games.Events
{
    public class PlayerWon : DomainEvent<PlayerWon>
    {
        public override object AggregateId { get; }
        public PlayerId PlayerId { get; }
        public int Amount { get; }

        public PlayerWon(
            GameId gameId, PlayerId playerId, int amount)
        {
            AggregateId = gameId;
            PlayerId = playerId;
            Amount = amount;
        }

        protected override IEnumerable<object> GetAllAttributesToBeUsedForEquality()
        {
            return base.GetAllAttributesToBeUsedForEquality()
                .Union(new object[] { PlayerId, Amount });
        }
    }
}