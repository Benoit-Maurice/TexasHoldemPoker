﻿using System.Collections.Generic;
using TexasHoldemPoker.Games;
using TexasHoldemPoker.Games.Commands;
using TexasHoldemPoker.Games.Events;
using TexasHoldemPoker.Players;
using TexasHoldemPoker.Tests.Framework;
using TexasHoldemPoker.Tests.Games.Samples;
using Xunit;
using Xunit.Abstractions;
using static TexasHoldemPoker.Games.Domain.BettingRounds.BettingRoundName;

namespace TexasHoldemPoker.Tests.Games.Scenarios
{
    public class FlopShould : Feature
    {
        public readonly GameSamples game = new GameSamples();

        public FlopShould(ITestOutputHelper outputHelper) : base(outputHelper)
        {
        }

        [Fact]
        public void Start_When_Preflop_BettingRound_Is_Over() =>
            Given.A<Game>()
                .WithHistory(game.Started)
                .And(game.Preflop.Started)
                .And(game.Preflop.UTGCalled)
                .And(game.Preflop.DealerFolded)
                .And(game.Preflop.SBCalled)
                .When(new PlayerChecks(game.Id, game.Players.BigBlindId))
                .Then(
                    CheckNewHistory.Skip(2)
                        .Equals(
                            new BettingRoundStarted(game.Id, Flop, game.Players.DealerId)));

        [Fact]
        public void Deal_Three_Community_Cards_When_Starting() =>
            Given.A<Game>()
                .WithHistory(game.Started)
                .And(game.Preflop.Started)
                .And(game.Preflop.UTGCalled)
                .And(game.Preflop.DealerFolded)
                .And(game.Preflop.SBCalled)
                .When(new PlayerChecks(game.Id, game.Players.BigBlindId))
                .Then(
                    CheckNewHistory.Skip(3)
                        .Equals(
                            new CardBurnt(game.Id, game.Cards.BurntBeforeFlop),
                            new FlopDealt(game.Id, game.Cards.Flop)));

        [Fact]
        public void Start_SmallBlind_PlayerTurn_When_Started() =>
            Given.A<Game>()
                .WithHistory(game.Started)
                .And(game.Preflop.Started)
                .And(game.Preflop.UTGCalled)
                .And(game.Preflop.DealerFolded)
                .And(game.Preflop.SBCalled)
                .When(new PlayerChecks(game.Id, game.Players.BigBlindId))
                .Then(
                    CheckNewHistory.EndsWith(
                        new PlayerTurnStarted(game.Id, game.Players.SmallBlindId)));

        [Fact]
        public void Fire_A_PlayerBet_Event_When_SmallBlind_Player_Bet() =>
            Given.A<Game>()
                .WithHistory(game.Started)
                .And(game.Preflop.Happened)
                .And(game.Flop.Started)
                .When(new PlayerBets(game.Id, game.Players.SmallBlindId, game.Blinds.Big * 2))
                .Then(
                    CheckNewHistory.StartsWith(
                        new PlayerBet(game.Id, game.Players.SmallBlindId, game.Blinds.Big * 2),
                        new PlayerTurnStarted(game.Id, game.Players.BigBlindId)));

        [Fact]
        public void Fire_A_PlayerFolded_Event_When_BigBlind_Player_Folds() =>
            Given.A<Game>()
                .WithHistory(game.Started)
                .And(game.Preflop.Happened)
                .And(game.Flop.Started)
                .And(game.Flop.SBBet)
                .When(new PlayerFolds(game.Id, game.Players.BigBlindId))
                .Then(
                    CheckNewHistory.StartsWith(
                        new PlayerFolded(game.Id, game.Players.BigBlindId),
                        new PlayerTurnStarted(game.Id, game.Players.UnderTheGunId)));

        [Fact]
        public void Fire_A_PlayerRaised_Event_When_UnderTheGun_Player_Raises() =>
            Given.A<Game>()
                .WithHistory(game.Started)
                .And(game.Preflop.Happened)
                .And(game.Flop.Started)
                .And(game.Flop.SBBet)
                .And(game.Flop.BBFolded)
                .When(new PlayerRaises(game.Id, game.Players.UnderTheGunId, game.Blinds.Big * 4))
                .Then(
                    CheckNewHistory.StartsWith(
                        new PlayerRaised(game.Id, game.Players.UnderTheGunId, game.Blinds.Big * 4),
                        new PlayerTurnStarted(game.Id, game.Players.SmallBlindId)));

        [Fact]
        public void Fire_A_PlayerCalled_Event_when_SmallBlind_Player_Calls() =>
            Given.A<Game>()
                .WithHistory(game.Started)
                .And(game.Preflop.Happened)
                .And(game.Flop.Started)
                .And(game.Flop.SBBet)
                .And(game.Flop.BBFolded)
                .And(game.Flop.UTGRaised)
                .When(new PlayerCalls(game.Id, game.Players.SmallBlindId))
                .Then(
                    CheckNewHistory.StartsWith(
                        new PlayerCalled(game.Id, game.Players.SmallBlindId)));

        [Fact]
        public void End_When_All_Players_Have_Finished() =>
            Given.A<Game>()
                .WithHistory(game.Started)
                .And(game.Preflop.Happened)
                .And(game.Flop.Started)
                .And(game.Flop.SBBet)
                .And(game.Flop.BBFolded)
                .And(game.Flop.UTGRaised)
                .When(new PlayerCalls(game.Id, game.Players.SmallBlindId))
                .Then(
                    CheckNewHistory.Skip(1)
                        .Equals(
                            new BettingRoundIsOver(game.Id, Flop,
                                new Dictionary<PlayerId, int>()
                                {
                                    [game.Players.UnderTheGunId] = game.Blinds.Big * 2 + game.Blinds.Big * 4,
                                    [game.Players.SmallBlindId] = game.Blinds.Big * 2 + game.Blinds.Big * 4,
                                })));
    }
}