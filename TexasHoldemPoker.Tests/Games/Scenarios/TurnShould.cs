﻿using System.Collections.Generic;
using TexasHoldemPoker.Games;
using TexasHoldemPoker.Games.Commands;
using TexasHoldemPoker.Games.Events;
using TexasHoldemPoker.Players;
using TexasHoldemPoker.Tests.Framework;
using TexasHoldemPoker.Tests.Games.Samples;
using Xunit;
using Xunit.Abstractions;
using static TexasHoldemPoker.Games.Domain.BettingRounds.BettingRoundName;

namespace TexasHoldemPoker.Tests.Games.Scenarios
{
    public class TurnShould : Feature
    {
        public readonly GameSamples game = new GameSamples();

        public TurnShould(ITestOutputHelper outputHelper) : base(outputHelper)
        {
        }

        [Fact]
        public void Start_When_Flop_BettingRound_Is_Over() =>
            Given.A<Game>()
                .WithHistory(game.Started)
                .And(game.Preflop.Happened)
                .And(game.Flop.Started)
                .And(game.Flop.SBBet)
                .And(game.Flop.BBFolded)
                .And(game.Flop.UTGRaised)
                .When(new PlayerCalls(game.Id, game.Players.SmallBlindId))
                .Then(
                    CheckNewHistory.Skip(2)
                        .Equals(
                            new BettingRoundStarted(game.Id, Turn, game.Players.DealerId)));

        [Fact]
        public void Deal_A_Community_Card_When_Starting() =>
            Given.A<Game>()
                .WithHistory(game.Started)
                .And(game.Preflop.Happened)
                .And(game.Flop.Started)
                .And(game.Flop.SBBet)
                .And(game.Flop.BBFolded)
                .And(game.Flop.UTGRaised)
                .When(new PlayerCalls(game.Id, game.Players.SmallBlindId))
                .Then(
                    CheckNewHistory.Skip(3)
                        .Equals(
                            new CardBurnt(game.Id, game.Cards.BurntBeforeTurn),
                            new TurnDealt(game.Id, game.Cards.Turn)));

        [Fact]
        public void Start_SmallBlind_Player_Turn_When_Started() =>
            Given.A<Game>()
                .WithHistory(game.Started)
                .And(game.Preflop.Happened)
                .And(game.Flop.Started)
                .And(game.Flop.SBBet)
                .And(game.Flop.BBFolded)
                .And(game.Flop.UTGRaised)
                .When(new PlayerCalls(game.Id, game.Players.SmallBlindId))
                .Then(
                    CheckNewHistory.EndsWith(
                        new PlayerTurnStarted(game.Id, game.Players.SmallBlindId))
                );

        [Fact]
        public void Fire_PlayerChecked_When_SmallBlind_Checks() =>
            Given.A<Game>()
                .WithHistory(game.Started)
                .And(game.Preflop.Happened)
                .And(game.Flop.Happened)
                .And(game.Turn.Started)
                .When(new PlayerChecks(game.Id, game.Players.SmallBlindId))
                .Then(
                    CheckNewHistory.StartsWith(
                        new PlayerChecked(game.Id, game.Players.SmallBlindId),
                        new PlayerTurnStarted(game.Id, game.Players.UnderTheGunId)));

        [Fact]
        public void End_When_All_Players_Have_Finished() =>
            Given.A<Game>()
                .WithHistory(game.Started)
                .And(game.Preflop.Happened)
                .And(game.Flop.Happened)
                .And(game.Turn.Started)
                .And(game.Turn.SBChecked)
                .When(new PlayerChecks(game.Id, game.Players.UnderTheGunId))
                .Then(
                    CheckNewHistory.StartsWith(
                        new PlayerChecked(game.Id, game.Players.UnderTheGunId),
                        new BettingRoundIsOver(game.Id, Turn,
                            new Dictionary<PlayerId, int>()
                            )));
    }
}