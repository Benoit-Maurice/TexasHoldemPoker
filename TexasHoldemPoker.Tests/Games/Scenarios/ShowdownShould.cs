﻿using TexasHoldemPoker.Games;
using TexasHoldemPoker.Games.Commands;
using TexasHoldemPoker.Games.Events;
using TexasHoldemPoker.Tests.Framework;
using TexasHoldemPoker.Tests.Games.Samples;
using Xunit;
using Xunit.Abstractions;
using static TexasHoldemPoker.Games.Domain.BettingRounds.BettingRoundName;

namespace TexasHoldemPoker.Tests.Games.Scenarios
{
    public class ShowdownShould : Feature
    {
        public readonly GameSamples game = new GameSamples();

        public ShowdownShould(ITestOutputHelper outputHelper) : base(outputHelper)
        {
        }

        [Fact]
        public void Start_When_River_BettingRound_Is_Over() =>
            Given.A<Game>()
                .WithHistory(game.Started)
                .And(game.Preflop.Happened)
                .And(game.Flop.Happened)
                .And(game.Turn.Happened)
                .And(game.River.Started)
                .And(game.River.SBWentAllIn)
                .When(new PlayerCalls(game.Id, game.Players.UnderTheGunId))
                .Then(
                    CheckNewHistory.Skip(2)
                                .Equals(
                                    new BettingRoundStarted(game.Id, Showdown, game.Players.DealerId)
                        ));

        [Fact]
        public void Pay_Winners() =>
            Given.A<Game>()
                .WithHistory(game.Started)
                .And(game.Preflop.Happened)
                .And(game.Flop.Happened)
                .And(game.Turn.Happened)
                .And(game.River.Started)
                .And(game.River.SBWentAllIn)
                .When(new PlayerCalls(game.Id, game.Players.UnderTheGunId))
                .Then(
                    CheckNewHistory.Skip(3)
                                .Equals(new PlayerWon(game.Id, game.Players.UnderTheGunId, 61_000))
                        );
    }
}