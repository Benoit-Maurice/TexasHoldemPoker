﻿using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Xunit;

namespace TexasHoldemPoker.Tests.Framework
{
    public static class StackFrameExtensionMethod
    {
        public static MethodBase FindCallingTestMethod(this StackTrace stackTrace)
        {
            return FindCallingTestMethodFrame(stackTrace)?.GetMethod();
        }

        private static StackFrame FindCallingTestMethodFrame(StackTrace stackTrace)
        {
            return stackTrace.GetFrames().Where(f => IsATest(f.GetMethod())).SingleOrDefault();
        }

        private static bool IsATest(MethodBase methodBase)
        {
            var attributes = methodBase.GetCustomAttributes<FactAttribute>();
            return attributes.Any();
        }
    }
}
