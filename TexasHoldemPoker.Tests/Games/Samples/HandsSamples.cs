﻿using System.Linq;
using TexasHoldemPoker.Cards;
using TexasHoldemPoker.Hands;
using static TexasHoldemPoker.Cards.Rank;
using static TexasHoldemPoker.Cards.Suit;

namespace TexasHoldemPoker.Tests.Games.Samples
{
    public class HandsSamples
    {
        public readonly Hand SmallBlind = Hand(Ace.Of(Heart), Two.Of(Spade));
        public readonly Hand BigBlind = Hand(Jack.Of(Heart), Three.Of(Diamond));
        public readonly Hand UnderTheGun = Hand(King.Of(Club), Queen.Of(Spade));
        public readonly Hand Dealer = Hand(Ten.Of(Spade), Seven.Of(Spade));

        public Card[] AllCards => FromHands(SmallBlind, BigBlind, UnderTheGun, Dealer);

        internal static Card[] FromHands(params Hand[] hands)
        {
            var cards = hands.Select(hand => hand.First).ToList();
            cards.AddRange(hands.Select(hand => hand.Last));
            return cards.ToArray();
        }

        private static Hand Hand(Card card1, Card card2) => new Hand(2).AddCard(card1).AddCard(card2);
    }
}
