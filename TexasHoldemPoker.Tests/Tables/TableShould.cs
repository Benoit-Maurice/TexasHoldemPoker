﻿using TexasHoldemPoker.Tables;
using TexasHoldemPoker.Tables.Commands;
using TexasHoldemPoker.Tables.Events;
using TexasHoldemPoker.Tables.Exceptions;
using TexasHoldemPoker.Tests.Framework;
using TexasHoldemPoker.Tests.Games;
using Xunit;
using Xunit.Abstractions;

namespace TexasHoldemPoker.Tests.Tables
{
    public class TableShould : Feature
    {
        private readonly TableSamples table = new TableSamples();

        public TableShould(ITestOutputHelper outputHelper) : base(outputHelper)
        {
        }

        [Fact]
        public void Fire_A_TableOpened_Event_When_Created() =>
            Given.A<Table>()
                 .When(new OpenNewTable(table.Id, 4, table.SmallBlind, table.BigBlind))
                 .Then(
                    CheckNewHistory.Is(
                        new TableOpened(table.Id, 4, table.SmallBlind, table.BigBlind)));

        [Fact]
        public void Fail_When_A_Player_Is_Already_Seated_At_The_Table() => 
            Given.A<Table>()
                .WithHistory(
                    new TableOpened(table.Id, table.HeadsUpGameNumberOfSeats, 
                        table.SmallBlind, table.BigBlind),
                    new PlayerJoined(table.Id, table.PlayerOne, table.PlayerOneStake)
                )
                .When(new PlayerJoins(table.Id, table.PlayerOne, table.PlayerOneStake))
                .ThenItFailsWith<PlayerAlreadySeated>();


        [Fact]
        public void Fail_When_A_Player_Tries_To_Join_A_Full_Table() => 
            Given.A<Table>()
                .WithHistory(
                    new TableOpened(table.Id, table.HeadsUpGameNumberOfSeats, table.SmallBlind, table.BigBlind),
                    new PlayerJoined(table.Id, table.PlayerOne, table.PlayerOneStake),
                    new PlayerJoined(table.Id, table.PlayerTwo, table.PlayerTwoStake)
                )
                .When(new PlayerJoins(table.Id, table.LatePlayer, table.LatePlayerStake))
                .ThenItFailsWith<TableIsFull>();

        [Fact]
        public void Fire_A_PlayerJoined_Event_When_A_New_Player_Joins_The_Table() => 
            Given.A<Table>()
                .WithHistory(
                    new TableOpened(table.Id, table.HeadsUpGameNumberOfSeats, table.SmallBlind, table.BigBlind)
                )
                .When(new PlayerJoins(table.Id, table.PlayerOne, table.PlayerOneStake))
                .Then(
                    CheckNewHistory.Is(
                        new PlayerJoined(table.Id, table.PlayerOne, table.PlayerOneStake)));
    }
}