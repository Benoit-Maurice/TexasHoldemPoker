﻿using System.Collections.Generic;
using NFluent;
using TexasHoldemPoker.Framework;
using Xunit;
using static ActionExtensionMethods;

namespace TexasHoldemPoker.Tests.Framework
{
    public class LoopShould
    {
        [Fact]
        public void Default_Enumeration()
        {
            var loop = new Loop<int>
            {
                1, 2, 3, 4
            };

            var result = new List<int>();

            foreach (var elt in loop)
            {
                result.Add(elt);
            }

            Check.That(result).ContainsExactly(1, 2, 3, 4);
        }

        [Fact]
        public void Enumeration_After_Shifting_Backward_N_Times_The_First_Element()
        {
            var loop = new Loop<int>
            {
                1, 2, 3, 4
            };

            Repeat(4,
                () => loop.ShiftBackward());

            var result = new List<int>();

            foreach (var elt in loop) result.Add(elt);

            Check.That(result).ContainsExactly(1, 2, 3, 4);
        }

        [Fact]
        public void Enumeration_After_Shifting_Backward_The_First_Element()
        {
            var loop = new Loop<int>
            {
                1, 2, 3, 4
            };

            loop.ShiftBackward();

            var result = new List<int>();

            foreach (var elt in loop)
            {
                result.Add(elt);
            }

            Check.That(result).ContainsExactly(4, 1, 2, 3);
        }

        [Fact]
        public void Enumeration_After_Shifting_Forward_N_Times_The_First_Element()
        {
            var loop = new Loop<int>
            {
                1, 2, 3, 4
            };

            Repeat(4, () => loop.ShiftForward());

            var result = new List<int>();

            foreach (var elt in loop)
            {
                result.Add(elt);
            }

            Check.That(result).ContainsExactly(1, 2, 3, 4);
        }

        [Fact]
        public void Enumeration_After_Shifting_Forward_The_First_Element()
        {
            var loop = new Loop<int>
            {
                1, 2, 3, 4
            };

            loop.ShiftForward();

            var result = new List<int>();

            foreach (var elt in loop)
            {
                result.Add(elt);
            }

            Check.That(result).ContainsExactly(2, 3, 4, 1);
        }
    }
}