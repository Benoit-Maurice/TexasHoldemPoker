﻿using NFluent;
using System.Diagnostics;
using Xunit;

namespace TexasHoldemPoker.Tests.Framework
{
    public class TextMethodBaseExtensionMethodShould
    {
        [Fact]
        public void Generate_a_readable_sentence_from_a_Method_name_and_its_Type_Name()
        {
            var method = new StackTrace().FindCallingTestMethod();

            var sentence = method.NameAsReadableSentence();

            Check.That(sentence).IsEqualTo("Text method base extension method should generate a readable sentence from a method name and its type name");
        }

    }
}
