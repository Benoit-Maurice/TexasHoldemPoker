﻿using NFluent;
using TexasHoldemPoker.Hands.HandValues;
using TexasHoldemPoker.Tests.Framework;
using Xunit;
using static TexasHoldemPoker.Tests.Hands.HandValues.HandValueFactory;

namespace TexasHoldemPoker.Tests.Hands.HandValues
{
    public class TwoPairsShould
    {
        [Fact]
        public void Beat_Another_TwoPairs_With_A_Smaller_High_Pair()
        {
            Check.That(This<TwoPairs>("A♦ K♣ K♥ A♣ Q♠"))
                .Beats(This<TwoPairs>("Q♦ J♣ Q♥ A♣ J♠"));
        }

        [Fact]
        public void Beat_Another_TwoPairs_With_The_Same_High_And_Low_Pairs_But_A_Smaller_Kicker()
        {
            Check.That(This<TwoPairs>("A♦ K♣ K♥ A♣ Q♠"))
                .Beats(This<TwoPairs>("A♦ A♣ K♥ 9♣ K♠"));
        }

        [Fact]
        public void Beat_Another_TwoPairs_With_The_Same_High_Pair_And_A_Smaller_Low_Pair()
        {
            Check.That(This<TwoPairs>("A♦ K♣ K♥ A♣ Q♠"))
                .Beats(This<TwoPairs>("A♦ A♣ J♥ 9♣ J♠"));
        }

        [Fact]
        public void Beat_The_Biggest_HighCard()
        {
            Check.That(This<TwoPairs>("A♦ J♣ K♥ A♣ J♠"))
                .Beats(This<HighCard>("A♦ J♣ K♥ 9♣ Q♠"));
        }

        [Fact]
        public void Beat_The_Biggest_Pair()
        {
            Check.That(This<TwoPairs>("A♦ J♣ K♥ A♣ J♠"))
                .Beats(This<Pair>("A♦ J♣ K♥ A♣ Q♠"));
        }

        [Fact]
        public void Not_Beat_Another_TwoPairs_With_The_Same_Strength()
        {
            Check.That(This<TwoPairs>("A♦ K♣ K♥ A♣ Q♠"))
                .Not.Beats(This<TwoPairs>("A♥ K♦ K♠ A♠ Q♦"));
        }
    }
}