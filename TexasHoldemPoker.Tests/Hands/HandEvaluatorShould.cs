﻿using System.Linq;
using NFluent;
using TexasHoldemPoker.Cards;
using TexasHoldemPoker.Hands;
using TexasHoldemPoker.Hands.HandValues;
using TexasHoldemPoker.Tests.Framework;
using Xunit;

namespace TexasHoldemPoker.Tests.Hands
{
    public class HandEvaluatorShould
    {
        public HandEvaluatorShould()
        {
            _cardParser = new StringToCardParser(new Deck());
            _parser = new StringToHandParser(_cardParser);
            _evaluator = new HandEvaluator();
        }

        private readonly StringToCardParser _cardParser;
        private readonly StringToHandParser _parser;
        private readonly HandEvaluator _evaluator;

        [Theory]
        [InlineData("2♣ 2♥ J♦ J♠ 4♣ 4♦ A♣", "J♦ J♠ 4♣ 4♦ A♣")]
        [InlineData("2♦ 2♥ 3♦ J♦ J♣ 4♦ A♦", "2♦ 3♦ 4♦ J♦ A♦")]
        [InlineData("A♣ 2♥ 3♦ 5♦ J♣ 4♦ A♦", "2♥ 3♦ 4♦ 5♦ A♣")]
        public void Find_The_Best_Five_Cards_Hand_Amongst_Seven_Cards(string cardsAsString, string expectedHandAsString)
        {
            var cards = cardsAsString.Split(' ')
                .Select(cs => _cardParser.Parse(cs)).ToList();

            var expectedHand = _parser.Parse(expectedHandAsString);

            var evaluatedHand = _evaluator.FindBestHand(cards);

            Check.That(evaluatedHand.Hand).IsEqualTo(expectedHand);
        }

        [Fact]
        public void Return_A_Flush_Category_When_Evaluating_A_Hand_With_Cards_Of_The_Same_Suit()
        {
            var hand = _parser.Parse("2♥ T♥ J♥ K♥ 7♥");
            var evaluatedHand = _evaluator.Evaluate(hand);

            Check.That(evaluatedHand).IsInstanceOf<Flush>();
            var flush = (Flush) evaluatedHand;

            Check.ThatEnum(flush.Suit).IsEqualTo(Suit.Heart);
        }

        [Fact]
        public void Return_A_Four_Of_A_Kind_Category_When_Evaluating_A_Hand_With_Four_Cards_Of_The_Same_Rank()
        {
            var hand = _parser.Parse("2♣ 2♥ 2♦ 2♠ 4♣");
            var evaluatedHand = _evaluator.Evaluate(hand);

            Check.That(evaluatedHand).IsInstanceOf<FourOfAKind>();
            var fourOfAKind = (FourOfAKind) evaluatedHand;

            Check.ThatEnum(fourOfAKind.Rank).IsEqualTo(Rank.Two);
        }

        [Fact]
        public void Return_A_Full_House_Category_When_Evaluating_A_Hand_With_A_Pair_And_A_Three_Of_A_Kind()
        {
            var hand = _parser.Parse("2♣ 2♥ 3♦ 3♠ 3♣");
            var evaluatedHand = _evaluator.Evaluate(hand);

            Check.That(evaluatedHand).IsInstanceOf<FullHouse>();
            var fullHouse = (FullHouse) evaluatedHand;

            Check.ThatEnum(fullHouse.ThreeOfAKind.Rank).IsEqualTo(Rank.Three);
            Check.ThatEnum(fullHouse.Pair.Rank).IsEqualTo(Rank.Two);
        }

        [Fact]
        public void Return_A_Full_House_Category_When_Evaluating_A_Hand_With_A_Three_Of_A_Kind_And_A_Pair()
        {
            var hand = _parser.Parse("4♣ 4♥ 3♦ 3♠ 3♣");
            var evaluatedHand = _evaluator.Evaluate(hand);

            Check.That(evaluatedHand).IsInstanceOf<FullHouse>();
            var fullHouse = (FullHouse) evaluatedHand;

            Check.ThatEnum(fullHouse.ThreeOfAKind.Rank).IsEqualTo(Rank.Three);
            Check.ThatEnum(fullHouse.Pair.Rank).IsEqualTo(Rank.Four);
        }

        [Fact]
        public void Return_A_Pair_Category_When_Evaluating_A_Hand_With_Two_Cards_Of_The_Same_Rank()
        {
            var hand = _parser.Parse("2♣ 2♥ J♦ Q♠ 4♣");
            var evaluatedHand = _evaluator.Evaluate(hand);

            Check.That(evaluatedHand).IsInstanceOf<Pair>();
            var pair = (Pair) evaluatedHand;

            Check.ThatEnum(pair.Rank).IsEqualTo(Rank.Two);
        }

        [Fact]
        public void Return_A_Straight_Category_When_Evaluating_A_Hand_With_Five_Cards_Following()
        {
            var hand = _parser.Parse("8♣ 9♥ T♦ J♠ Q♣");
            var evaluatedHand = _evaluator.Evaluate(hand);

            Check.That(evaluatedHand).IsInstanceOf<Straight>();
        }

        [Fact]
        public void Return_A_Straight_Category_When_Evaluating_A_Hand_With_Five_Cards_Following_And_Ending_With_An_Ace()
        {
            var hand = _parser.Parse("T♣ J♥ Q♦ K♠ A♣");
            var evaluatedHand = _evaluator.Evaluate(hand);

            Check.That(evaluatedHand).IsInstanceOf<Straight>();
        }

        [Fact]
        public void
            Return_A_Straight_Category_When_Evaluating_A_Hand_With_Five_Cards_Following_And_Starting_With_An_Ace()
        {
            var hand = _parser.Parse("A♣ 2♥ 3♦ 4♠ 5♣");
            var evaluatedHand = _evaluator.Evaluate(hand);

            Check.That(evaluatedHand).IsInstanceOf<Straight>();
        }

        [Fact]
        public void Return_A_StraightFlush_Category_When_Evaluating_A_Hand_That_Is_A_Straight_And_A_Flush()
        {
            var hand = _parser.Parse("T♥ J♥ Q♥ K♥ A♥");
            var evaluatedHand = _evaluator.Evaluate(hand);

            Check.That(evaluatedHand).IsInstanceOf<StraightFlush>();
            var straightFlush = (StraightFlush) evaluatedHand;

            Check.ThatEnum(straightFlush.Flush.Suit).IsEqualTo(Suit.Heart);
            Check.ThatEnum(straightFlush.Straight.HighestCard.Rank).IsEqualTo(Rank.Ace);
        }

        [Fact]
        public void Return_A_Three_Of_A_Kind_Category_When_Evaluating_A_Hand_With_Three_Cards_Of_The_Same_Rank()
        {
            var hand = _parser.Parse("2♦ 3♠ 3♥ 3♦ J♣");
            var evaluatedHand = _evaluator.Evaluate(hand);

            Check.That(evaluatedHand).IsInstanceOf<ThreeOfAKind>();
            var threeOfAKind = (ThreeOfAKind) evaluatedHand;

            Check.ThatEnum(threeOfAKind.Rank).IsEqualTo(Rank.Three);
        }

        [Fact]
        public void Return_A_Three_Of_A_Kind_Category_When_Evaluating_A_Hand_With_Three_Cards_Of_The_Same_Rank_2()
        {
            var hand = _parser.Parse("2♦ 2♠ 2♥ 3♦ J♣");
            var evaluatedHand = _evaluator.Evaluate(hand);

            Check.That(evaluatedHand).IsInstanceOf<ThreeOfAKind>();
            var threeOfAKind = (ThreeOfAKind) evaluatedHand;

            Check.ThatEnum(threeOfAKind.Rank).IsEqualTo(Rank.Two);
        }

        [Fact]
        public void Return_A_Three_Of_A_Kind_Category_When_Evaluating_A_Hand_With_Three_Cards_Of_The_Same_Rank_3()
        {
            var hand = _parser.Parse("2♦ 3♠ J♥ J♦ J♣");
            var evaluatedHand = _evaluator.Evaluate(hand);

            Check.That(evaluatedHand).IsInstanceOf<ThreeOfAKind>();
            var threeOfAKind = (ThreeOfAKind) evaluatedHand;

            Check.ThatEnum(threeOfAKind.Rank).IsEqualTo(Rank.Jack);
        }

        [Fact]
        public void Return_A_Two_Pairs_Category_When_Evaluating_A_Hand_With_Two_Pairs()
        {
            var hand = _parser.Parse("2♣ 2♥ J♦ J♠ 4♣");
            var evaluatedHand = _evaluator.Evaluate(hand);

            Check.That(evaluatedHand).IsInstanceOf<TwoPairs>();
            var twoPairs = (TwoPairs) evaluatedHand;

            Check.ThatEnum(twoPairs.HighestPair.Rank).IsEqualTo(Rank.Jack);
            Check.ThatEnum(twoPairs.LowestPair.Rank).IsEqualTo(Rank.Two);

            Check.ThatEnum(twoPairs.RemainingCard.Cards[0].Rank).IsEqualTo(Rank.Four);
        }
    }
}