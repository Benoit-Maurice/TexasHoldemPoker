﻿using System;
using System.Runtime.Serialization;

namespace TexasHoldemPoker.Framework
{
    [Serializable]
    public class FailToDispatchCommand : Exception
    {
        public FailToDispatchCommand()
        {
        }

        public FailToDispatchCommand(string message) : base(message)
        {
        }

        public FailToDispatchCommand(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected FailToDispatchCommand(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}