﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace TexasHoldemPoker.Framework
{
    public class History : IEnumerable<DomainEvent>
    {
        public static History Clean = new History(Enumerable.Empty<DomainEvent>());

        private readonly IEnumerable<DomainEvent> history;

        public History(params DomainEvent[] history)
        {
            this.history = history;
        }

        public History(IEnumerable<DomainEvent> history)
        {
            this.history = history;
        }

        public History Then(Func<History> then) 
        {
            return new History(this.history.Union(then().history));
        }

        public IEnumerator<DomainEvent> GetEnumerator()
        {
            return history.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}