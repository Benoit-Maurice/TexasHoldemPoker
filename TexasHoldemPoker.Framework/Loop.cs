﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace TexasHoldemPoker.Framework
{
    public class Loop<T> : ICollection<T>
    {
        private readonly List<T> content;
        private int beginningOfTheLoop;

        public T this[int index] => content[index];

        public Loop(IEnumerable<T> content)
        {
            this.content = content.ToList();
        }

        public Loop(int capacity = 0)
        {
            content = new List<T>(capacity);
        }

        public int Count => content.Count;

        public bool IsReadOnly { get; } = false;

        public void Add(T item)
        {
            content.Add(item);
        }

        public void Clear()
        {
            content.Clear();
        }

        public bool Contains(T item)
        {
            return content.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            using (var enumerator = GetEnumerator())
            {
                var index = arrayIndex;

                while (enumerator.MoveNext())
                {
                    array[index++] = enumerator.Current;
                }
            }
        }

        public bool Remove(T item)
        {
            return content.Remove(item);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new LoopEnumerator(this, beginningOfTheLoop);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void ShiftForward()
        {
            beginningOfTheLoop++;
            if (beginningOfTheLoop > content.Count) beginningOfTheLoop = 0;
        }

        public void ShiftBackward()
        {
            beginningOfTheLoop--;
            if (beginningOfTheLoop < 0) beginningOfTheLoop = content.Count - 1;
        }

        private class LoopEnumerator : IEnumerator<T>
        {
            private readonly int initialIndex;

            private readonly Loop<T> loop;
            private int index;
            private int numberOfElementsEnumerated;

            public LoopEnumerator(Loop<T> loop, int index)
            {
                this.loop = loop;
                this.index = index - 1;
                initialIndex = index;
            }

            public T Current => loop.content[index];

            object IEnumerator.Current => Current;

            public void Dispose()
            {
            }

            public bool MoveNext()
            {
                index++;
                numberOfElementsEnumerated++;

                if (initialIndex != 0 && index >= loop.content.Count) index = 0;

                return numberOfElementsEnumerated <= loop.content.Count;
            }

            public void Reset()
            {
                numberOfElementsEnumerated = 0;
                index = initialIndex;
            }
        }
    }
}