﻿namespace TexasHoldemPoker.Framework
{
    public interface IHandle<in TCommand>
        where TCommand : Command
    {
        void Handle(TCommand command);
    }
}