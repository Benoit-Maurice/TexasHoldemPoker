﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace TexasHoldemPoker.Framework
{
    public static class ObjectExtensionMethods
    {
        public static string ToReadableSentence(this object instance, bool withAggregateId = false,
            IEnumerable<string> excludedPropertiesNames = null)
        {
            var commandType = instance.GetType();
            var name = commandType.Name;

            var properties = commandType.GetProperties();

            var excludedProperties = new List<PropertyInfo>();

            if (excludedPropertiesNames != null)
            {
                excludedProperties.AddRange(excludedPropertiesNames.Select(n => properties.Single(p => p.Name == n)));
            }

            PropertyInfo aggregateIdProperty = null;

            if (instance is Command)
            {
                aggregateIdProperty = properties.Single(p => p.Name == nameof(Command.AggregateId));
                excludedProperties.Add(aggregateIdProperty);
            }

            if (instance is DomainEvent)
            {
                aggregateIdProperty = properties.Single(p => p.Name == nameof(DomainEvent.AggregateId));
                excludedProperties.Add(aggregateIdProperty);
            }

            var tokens = name.SplitOnUpperCaseLetters()
                             .Select(token => FindMatchingProperty(token, properties, instance, excludedProperties));

            var additionalProperties = properties
                                .Except(excludedProperties)
                                .Select(p => ReadableProperty(p, instance));

            var sentence = string.Join(" ", tokens);

            if(additionalProperties.Any())
            { 
                sentence += $" with {string.Join(" - ", additionalProperties.OrderBy(p => p))}";
            }

            if (!withAggregateId || aggregateIdProperty == null)
            {
                return sentence;
            }

            var aggregateIdValue = aggregateIdProperty.GetValue(instance);
            return $"[{aggregateIdValue}] - {sentence}";
        }

        private static string ReadableProperty(PropertyInfo property, object @object)
        {
            var propertyValue = property.GetValue(@object);
            return $"{property.Name}={SerializePropertyValue(propertyValue, property.PropertyType)}";
        }

        private static object SerializePropertyValue(object propertyValue, Type propertyType)
        {
            if (propertyValue == null)
            {
                return "<null>";
            }

            if (typeof(IDictionary).IsAssignableFrom(propertyType))
            {
                return SerializeDictionaryPropertyValue(propertyValue);
            }

            if (typeof(IEnumerable).IsAssignableFrom(propertyType)
                && propertyType != typeof(string))
            {
                return SerializeEnumerablePropertyValue(propertyValue);
            }

            return propertyValue.ToString();
        }

        private static object SerializeDictionaryPropertyValue(object propertyValue)
        {
            var dictionaryPropertyValue = (IDictionary)propertyValue;

            var itemsAsString = new List<string>();

            foreach (var key in dictionaryPropertyValue.Keys)
            {
                itemsAsString.Add($"[{key}]={dictionaryPropertyValue[key]}");
            }

            return "{" + string.Join(",", itemsAsString) + "}";
        }

        private static object SerializeEnumerablePropertyValue(object propertyValue)
        {
            var enumerablePropertyValue = (IEnumerable)propertyValue;

            var itemsAsString = new List<string>();

            foreach (var item in enumerablePropertyValue)
            {
                itemsAsString.Add(item.ToString());
            }

            return "[" + string.Join(",", itemsAsString) + "]";
        }

        private static string FindMatchingProperty(
            string token, PropertyInfo[] properties, object instance, List<PropertyInfo> excludedProperties)
        {
            foreach (var property in properties)
            {
                var splittedPropertyName = property.Name.SplitOnUpperCaseLetters()
                                                        .Select(t => t.ToLowerInvariant())
                                                        .ToList();

                if (splittedPropertyName.Count == 2)
                {
                    if (splittedPropertyName.Contains(token.ToLowerInvariant()) &&
                        splittedPropertyName.Contains("id"))
                    {
                        excludedProperties.Add(property);
                        var value = property.GetValue(instance);
                        return $"{token.Capitalize()}(Id={value})";
                    }
                    else if (splittedPropertyName.Contains(token.ToLowerInvariant()) &&
                        splittedPropertyName.Contains("value"))
                    {
                        excludedProperties.Add(property);
                        var value = property.GetValue(instance);
                        return $"{token.Capitalize()}(Value={value})";
                    }
                }
                else if (splittedPropertyName.Count == 1)
                {
                    if (token.ToLowerInvariant() == splittedPropertyName.Single())
                    {
                        excludedProperties.Add(property);
                        var value = property.GetValue(instance);
                        return $"{token.Capitalize()}({value})";
                    }
                }
            }

            return token;
        }
    }
}
