﻿using System;
using System.Collections.Generic;
using System.Linq;

public static class IEnumerableExtensionMethods
{
    public static IEnumerable<IEnumerable<T>> CombinationsOf<T>(this IEnumerable<T> elements, int k)
    {
        return k == 0
            ? new[] {new T[0]}
            : elements.SelectMany((e, i) =>
                elements.Skip(i + 1).CombinationsOf(k - 1).Select(c => new[] {e}.Concat(c)));
    }

    public static T[][] Split<T>(this T[] array, int chunkSize)
    {
        if (array.Length < chunkSize)
        {
            var soloResult = new T[1][];
            soloResult[0] = array;
            return soloResult;
        }

        var numberOfChunk = (int) Math.Ceiling((double) (array.Length / chunkSize));

        var result = new T[numberOfChunk][];

        var elementIndex = 0;

        for (var chunkIndex = 0; chunkIndex < numberOfChunk; chunkIndex++)
        {
            result[chunkIndex] = new T[chunkSize];

            for (var elementIndexInChunk = 0; elementIndexInChunk < chunkSize; elementIndexInChunk++)
                result[chunkIndex][elementIndexInChunk] = array[elementIndex++];
        }

        return result;
    }

    public static void ForEach<T>(this IEnumerable<T> elements, Action<T> action)
    {
        elements.ToList().ForEach(action);
    }
}