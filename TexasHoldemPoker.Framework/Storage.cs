﻿namespace TexasHoldemPoker.Framework
{
    public interface Storage
    {
        SearchResult<T> SearchById<T>(object id)
            where T : Aggregate;

        T Create<T>()
            where T : Aggregate;
    }
}
