﻿using System.Collections.Generic;
using Value;

namespace TexasHoldemPoker.Framework
{
    public interface DomainEvent
    {
        object AggregateId { get; }
    }

    public abstract class DomainEvent<T> : ValueType<T>, DomainEvent
    {
        public abstract object AggregateId { get; }

        protected override IEnumerable<object> GetAllAttributesToBeUsedForEquality()
        {
            return new[] { AggregateId };
        }

        public override string ToString()
        {
            return this.ToReadableSentence(withAggregateId:true);
        }
    }
}